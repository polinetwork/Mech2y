# Miglioramento continuo

* MSE è la stima dell'errore del modello
* All'aumentare della varianza sono necessarie un numero maggiore di repliche
* Se il p-value del test for equal variances è basso, significa che mancano dei termini per poter descrivere correttamente il modello
* Un modello è additivo se non è significativa l'interazione tra i fattori
* Se un modello è addittivo, i fattori possono essere analizzati separatamente
* Se il modello è additivo, l'alpha è uguale all'alpha di famiglia, altrimenti bisogna dividerlo per il numero di confronti (famiglie)
![title](file:///Users/ftrava/Desktop/Screen%20Shot%202015-02-03%20at%2018.27.41.png)
* BIBD Balanced Incomplete Block Design si usa quando si sospetta che un fattore disturbi (fattore di blocco) e non è possibile sperimentare tutti i livelli del fattore di interesse nel blocco
* Se non si hanno repliche non si puo fare l'individual value plot
* Il Blocking è una tecnica di sperimentazione che serve ad individuare fattori di disturbo, ovvero fattori che influenzano la risposta ma che non sono di nostro interesse
![title](file:///Users/ftrava/Desktop/Screen%20Shot%202015-02-03%20at%2018.35.54.png)