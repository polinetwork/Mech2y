function [z,t,r]=fromxyztoztr(x,y,z)

% function [z,t,r]=fromxyztoztr[x,y,z]
% this function converts cartesian coordinates into sylindrical coordinates
% input: x,y,z, cartesian coordinates (can be column vectors)
% output: z,t,r, relate cylindrical coordinates

% z=z
z=z; % this is absolutely superfluous, of course

% r=sqrt(x^2+y^2)
r=sqrt(x.^2+y.^2);

% t=atan(y/x), if either x and y are in the first or forth quadrant, else
% t=atan(y/x)+pi

t=atan(y./x);
t(x<0)=t(x<0)+pi;