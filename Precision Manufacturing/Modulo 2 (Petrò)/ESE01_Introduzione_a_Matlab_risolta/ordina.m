function sorted=ordina(unsorted)

% function sorted=ordina(unsorted)
% this function sorts a vector using linear insertion
% input: unsorted, vector to sort
% output: sorted, sorted vector

% Initialize sorted
sorted=zeros(size(unsorted));
sorted=sorted(:);
sorted(1)=unsorted(1);

% now start linear insertion
for h=2:length(unsorted)
    for k=h-1:-1:1
        if unsorted(h)>sorted(k)
            sorted=[sorted(1:k); unsorted(h); sorted(k+1:end-1)];
            break
        end
        if k==1
            sorted=[unsorted(h); sorted(1:end-1)];
        end
    end
end