function [p_values,n_values]=split_pn(in_values)

% function [p_values,nvalues]=split_pn(in_values)
% this function takes a vector and separates positive values from negative
% values
% input: in_values, values to be separated
% output: p_values, positive values
%         n_values, negative values

p_values=zeros(size(in_values));
n_values=zeros(size(in_values));
indexp=1;
indexn=1;
for h=in_values(:)'
    if h>0
        p_values(indexp)=h;
        indexp=indexp+1;
    elseif h<0
        n_values(indexn)=h;
        indexn=indexn+1;
    end
end
p_values(indexp:end)=[];
n_values(indexn:end)=[];